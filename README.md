# prolog

Logic programming language associated with artificial intelligence and computational linguistics. https://en.m.wikipedia.org/wiki/Prolog

# Tutorial
* [Learn X in Y minutes, where X=prolog](https://learnxinyminutes.com/docs/prolog/)
* [three clause meta-interpreter
  ](https://google.com/search?q=three+clause+meta-interpreter)

# Try it online
* https://tio.run/#prolog-swi
* https://tio.run/#prolog-ciao

# Implementations
* SWI
* [prolog](http://hackage.haskell.org/package/prolog): A Prolog interpreter written in Haskell.
* [*Programming in Schelog*](http://ds26gte.github.io/schelog/index.html)
  (Prolog in Scheme)

# Books
* [*The Power of Prolog*
  ](https://www.metalevel.at/prolog)
  (2025) Markus Triska
* [*Programmer Passport: Prolog*
  ](https://pragprog.com/titles/passprol/programmer-passport-prolog/)
  2022 Bruce Tate (The Pragmatic Bookshelf)
* [*Logic Programming with Prolog*
  ](https://link.springer.com/book/10.1007/978-1-4471-5487-7)
  2013 Max Bramer (Springer Nature)
* [*Seven Languages in Seven Weeks
A Pragmatic Guide to Learning Programming Languages*
  ](https://pragprog.com/titles/btlang/seven-languages-in-seven-weeks/)
  2010 Bruce Tate (The Pragmatic Bookshelf)

# Scientific articles
* [*Fifty Years of Prolog and Beyond*
  ](https://cambridge.org/core/journals/theory-and-practice-of-logic-programming/article/fifty-years-of-prolog-and-beyond/3A5329B6E3639879301A6D44346FD1DD#s2-4-3)
  2022-11 (Theory and Practice of Logic Programming)
* [*Prolog Systems*
  ](https://www.cambridge.org/core/journals/theory-and-practice-of-logic-programming/issue/91130C3064D9656924B08580BD645FBF)
  2012-01 (Theory and Practice of Logic Programming)
* [*SWI-Prolog*
  ](https://www.cambridge.org/core/journals/theory-and-practice-of-logic-programming/article/abs/swiprolog/1A18020C8CA2A2EE389BE6A714D6A148)
  2012-01 (Theory and Practice of Logic Programming)
* [*An overview of Ciao and its design philosophy*
  ](https://www.cambridge.org/core/journals/theory-and-practice-of-logic-programming/article/abs/an-overview-of-ciao-and-its-design-philosophy/5CEA99C1E160B3D8EBE50AB9A124D969)
  2012-01 (Theory and Practice of Logic Programming)
* [*The YAP Prolog system*
  ](https://www.cambridge.org/core/journals/theory-and-practice-of-logic-programming/article/abs/yap-prolog-system/82CF24BE0DD4211796D5482A0B1EB9F4)
  2012-01 (Theory and Practice of Logic Programming)

# Related languages
## Datalog
* [*Datalog*](https://en.wikipedia.org/wiki/Datalog)

## Mercury

# Packages
## Alpine
## Debian
## Fedora
[Source Package pl](https://packages.fedoraproject.org/pkgs/pl/)
* [swi-prolog-core](https://packages.fedoraproject.org/pkgs/pl/swi-prolog-core)
* [swi-prolog-nox](https://packages.fedoraproject.org/pkgs/pl/swi-prolog-nox)
* [swi-prolog-core-packages](https://packages.fedoraproject.org/pkgs/pl/swi-prolog-core-packages)